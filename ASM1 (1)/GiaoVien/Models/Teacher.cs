﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GiaoVien.Models
{
    public class Teacher : Worker
    {
        private double HeSoLuong { get; set; }

        public Teacher(string hoten,DateTime namsinh, double luongcoban, double hesoluong)
        {
            HoTen = hoten;
            NamSinh = namsinh;  
            LuongCoBan = luongcoban;
            HeSoLuong = hesoluong;
        }

        public void NhapThongtin(double hesoluong) => HeSoLuong =hesoluong;

        public string XuatThongTin() => "Ho ten la: " + HoTen + ", nam sinh: " + NamSinh
            + ", luong co ban: " + LuongCoBan + ", he so luong: " +HeSoLuong;

        public double XuLy() => HeSoLuong +  0.6;
        public double TinhLuong() => LuongCoBan * HeSoLuong*1.25;

        
    }
}
