﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GiaoVien.Models
{
    public class Worker
    {
        protected string HoTen { get; set; }
        protected DateTime? NamSinh { get; set; }
        protected double LuongCoBan { get; set; }

        public Worker()
        {

        }

        public Worker(string hoTen, DateTime namSinh, double luong)
        {
            HoTen = hoTen;
            NamSinh = namSinh;
            LuongCoBan = luong;
        }

        public Worker NhapThongTin(string hoTen, DateTime namSinh, double luong)
        => new Worker(hoTen, namSinh, luong) { };   
        public double TinhLuong() => LuongCoBan;
        public String XuatThongTin() => "Ho ten la: " + HoTen +", nam sinh: " +NamSinh
            +", luong co ban: " +LuongCoBan;

    }
}
